""" Cleaning the description field in the input txt file
"""

import sys
import os

import pandas as pd

def print_help():
    """ Print the help message for the programme
    """
    print('Marine Citation Cleaner')
    print('-----------------------')
    print('')
    print('Cleaning the description field of the input text file')
    print('')
    print('Usage: ')
    print('python clean.py input_file output_file')
    print('')
    print('- input_file must be tab separated')
    print('- output_file will be xls or xlsx (given by file ending)')
    print('  or a tab separated text file for any other file ending)')
    print('')
    print('NB: existing output_file will be overwritten!')


def convert(input_file, output_file):
    df = pd.read_csv(input_file, sep='\t')
    extracts = ['Title', 'Authors', 'Source']
    for pos, nn in enumerate(extracts):
        df.insert(pos+2, nn, df['description'].str.extract('{}:</td><td>(.*?)</td>'.format(nn)))

    if os.path.splitext(output_file)[-1] in ['.xls', '.xlsx']:
        df.to_excel(output_file, sheet_name='literature', index=False)
    else:
        df.to_csv(output_file, sep='\t', index=False)
    print('Done - processed {} lines. Have a nice day'.format(len(df)))
    

if __name__ == '__main__':
    if len(sys.argv) == 1 or sys.argv[1] in ['-h', '--help']:
        print_help()
        sys.exit(0)
    if len(sys.argv) == 2:
        print('Output file name missing! Use clean.py -h for help')
        print('**************************')
        sys.exit(1)
    else:
        convert(sys.argv[1], sys.argv[2])
        sys.exit(0)
